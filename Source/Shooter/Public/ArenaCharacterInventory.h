// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ArenaCharacterInventory.generated.h"

class UArenaCharacterStats;
class AArenaItem;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SHOOTER_API UArenaCharacterInventory : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UArenaCharacterInventory();

	virtual void InitializeComponent() override;
	virtual void UninitializeComponent() override;

	void AddItem(AArenaItem* i_Item);

protected:
	UPROPERTY(Transient)
	UArenaCharacterStats* CharacterStats = nullptr;
		
};
