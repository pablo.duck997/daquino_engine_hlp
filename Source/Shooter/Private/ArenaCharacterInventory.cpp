// Fill out your copyright notice in the Description page of Project Settings.


#include "ArenaCharacterInventory.h"
#include "ArenaCharacterStats.h"
#include "ArenaItem.h"
#include "ArenaItemStatModifier.h"
#include "ArenaItemDataAsset.h"

#include "ArenaLogContext.h"


// Sets default values for this component's properties
UArenaCharacterInventory::UArenaCharacterInventory()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	bWantsInitializeComponent = true;

	// ...
}

void UArenaCharacterInventory::InitializeComponent() 
{
	Super::InitializeComponent();

	AActor* Owner = GetOwner();
	if(Owner != nullptr)
	{
		CharacterStats = Owner->FindComponentByClass<UArenaCharacterStats>();
	}

}

void UArenaCharacterInventory::UninitializeComponent() 
{
	Super::UninitializeComponent();
	CharacterStats = nullptr;
}

void UArenaCharacterInventory::AddItem(AArenaItem* i_Item)
{
	//if (CharacterStats == nullptr) 
	//{
	//	CharacterStats = GetOwner()->FindComponentByClass<UArenaCharacterStats>();
	//
	//}
	if (CharacterStats == nullptr) 
	{
		UE_LOG(LogArena, Warning, TEXT("Missing Character Stats"));
		return;
	}
	if (i_Item == nullptr) 
	{
		UE_LOG(LogArena, Warning, TEXT("Inavlid Item"));
		return;
	}

	UArenaItemDataAsset* DataAsset = i_Item->ItemData;

	if (DataAsset == nullptr) 
	{
		UE_LOG(LogArena, Warning, TEXT("Inavlid Item Data Asset"));
		return;
	}

	UE_LOG(LogArena, Log, TEXT("Added Item %s"), *DataAsset -> ItemName.ToString());

	UDataTable* Modifiers = DataAsset->StatModifier;

	if (Modifiers == nullptr)
	{
		UE_LOG(LogArena, Warning, TEXT("Inavlid Item Data Table"));
		return;
	}

	static const FString ContextString(TEXT("UArenaCharacterInventory::AddItem"));

	TArray<FArenaItemStatModifier*> Rows;
	Modifiers->GetAllRows(ContextString, Rows);

	int NumRows = Rows.Num();

	for (size_t RowIndex = 0; RowIndex < NumRows; ++RowIndex) 
	{
		FArenaItemStatModifier* Row = Rows[RowIndex];
		CharacterStats->UpdateStat(Row->StatName, Row->Value);
	}

}

