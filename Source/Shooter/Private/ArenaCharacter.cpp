// Fill out your copyright notice in the Description page of Project Settings.


#include "ArenaCharacter.h"
#include "ArenaCharacterStats.h"
#include "ArenaCharacterInventory.h"
#include "ArenaItem.h"

// Sets default values
AArenaCharacter::AArenaCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StatsComponent = CreateDefaultSubobject<UArenaCharacterStats>(TEXT("Stats"));
	InventoryComponent = CreateDefaultSubobject< UArenaCharacterInventory>(TEXT("Inventory"));
}

void AArenaCharacter::NotifyActorBeginOverlap(AActor* OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);

	if (OtherActor == nullptr)
	{
		return;
	}

	bool bIsAnItem = OtherActor->IsA<AArenaItem>();

	if (bIsAnItem) {
		AArenaItem* Item = Cast<AArenaItem>(OtherActor);
		InventoryComponent->AddItem(Item);

		Item->Destroy();
	}
}
