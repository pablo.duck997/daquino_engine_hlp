// Fill out your copyright notice in the Description page of Project Settings.


#include "ReflectionUtilsFunctionLibrary.h"

#include "GameFramework/Actor.h"
#include "Components/ActorComponent.h"

UProperty* UReflectionUtilsFunctionLibrary::RetrieveProperty(UObject* InObject, const FString& InPath, void*& OutTarget) 
{
	ensureAlways(InObject);

	if(InObject == nullptr)
	return nullptr;

	FString ObjectString;
	FString PropertyString;
	bool bSucceded = InPath.Split(".", &ObjectString, &PropertyString);

	ensureAlways(bSucceded);

	UObject* TargetObj = nullptr;
	if (ObjectString.Equals("This", ESearchCase::IgnoreCase))
	{
		TargetObj = InObject;
	}
	else 
	{
		AActor* InActor = Cast<AActor>(InObject);

		ensureAlways(InActor);

		if (InActor != nullptr) 
		{
			TArray<UActorComponent*> Components;
			InActor->GetComponents(Components);

			for (size_t CompIndx = 0; CompIndx < Components.Num(); ++CompIndx) 
			{
				UActorComponent* Component = Components[CompIndx];
				if (ObjectString.Equals(Component->GetName()), ESearchCase::IgnoreCase) 
				{
					TargetObj = Component;
					break;
				}
			}
		}
	}

	OutTarget = TargetObj;

	FProperty* OutProperty;

	if (TargetObj == nullptr) 
	{
		UClass* TargetClass = TargetObj->GetClass();
		FName PropertyName = FName(*PropertyString);
		OutProperty = TargetClass->FindPropertyByName(PropertyName);
		return OutProperty;
	}
	else 
	{
		OutProperty = nullptr;
	}

	return OutProperty;
}

